#!/bin/bash
echo "searching for duplicate key bindings.."

find ~/.config/herbstluftwm -type f -exec grep -E -w -o "keybind \S*" {} \; | sort | uniq -d

echo "Done!"
